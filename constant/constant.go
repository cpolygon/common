package constant

import "gitlab.com/cpolygon/common/class"

//general constant
const (
	SUCCESS_CHECK = 0
)

//default config
const (
	BACKEND_PORT = 8008
)

// privilege code
const (
	Admin class.Privilege = -1
	User  class.Privilege = 0
	Baned class.Privilege = 1
)
