package class

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Name      string    `gorm:"unique;not null;size:16;index"`
	Privilege Privilege `gorm:"not null"`
	Password  string    `gorm:"not null"`
	Age       uint8
	Friends   uint
}
